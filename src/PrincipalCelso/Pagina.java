package PrincipalCelso;

import java.awt.Color;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Pagina extends JLabel{
    private ImageIcon iconePagina = new ImageIcon(getClass().getResource("/Imagens/Pagina.png"));
    private ImageIcon iconeCabecalho = new ImageIcon(getClass().getResource("/Imagens/Cabecalho.png"));
    private ImageIcon iconeApagar = new ImageIcon(getClass().getResource("/Imagens/Apagar.png"));
    private ImageIcon iconeApagarAC = new ImageIcon(getClass().getResource("/Imagens/Apagar AC.png"));
    
    private JLabel imageCabecalho = new JLabel(iconeCabecalho);
    private JLabel imageApagar = new JLabel(iconeApagar);
    
    private int pagN, colunas = 0;
    private int numPrimeiraQuest, numUltimaQuest;
    private JLabel numRodape = new JLabel("numero"), labelNumCol = new JLabel("   ");
    private boolean clicada;
    
    public Pagina(int numero, int colu){
        this.setIcon(iconePagina);
        this.setBounds(0,0,iconePagina.getIconWidth(), iconePagina.getIconHeight());
        imageCabecalho.setBounds(8,8,iconeCabecalho.getIconWidth(), iconeCabecalho.getIconHeight());
        
        if(colu == 1){
            this.labelNumCol.setText("Uma Coluna");
        }else if(colu == 2){
            this.labelNumCol.setText("Duas Colunas");
        }
        this.labelNumCol.setBounds((iconePagina.getIconWidth()-labelNumCol.getWidth()- 60)/2, 3, 120, 20);
        this.labelNumCol.setFont(new Font("TIMES_ROMAN", Font.BOLD, 11));
        this.labelNumCol.setForeground(Color.LIGHT_GRAY);
        this.labelNumCol.setVisible(false);
        add(labelNumCol);
        
        this.colunas = colu;
        this.clicada = false;
        this.numPrimeiraQuest = 0;
        this.numUltimaQuest = 0;
        numPrimeiraQuest = 0;
        numUltimaQuest = 0;
        this.pagN = numero;
        
        numRodape.setBounds(iconePagina.getIconWidth()-33,iconePagina.getIconHeight() - 25,100, 20);
        numRodape.setText(""+pagN);
        add(numRodape);
        numRodape.setVisible(false);
        
        imageApagar.setBounds(iconePagina.getIconWidth()-iconeApagar.getIconWidth()- 3,7,iconeApagar.getIconWidth(), iconeApagar.getIconHeight());
        imageApagar.setToolTipText("Apagar Página");
        add(imageApagar);
        imageApagar.setVisible(false);
        
        add(imageCabecalho);
    }
    
    public void mudaInfo(int numero){
        this.numRodape.setText(""+numero);
        this.setToolTipText("Página: "+numero);
        this.setPagN(numero);
    }

    public JLabel getLabelNumCol() {
        return labelNumCol;
    }

    public void setLabelNumCol(JLabel labelNumCol) {
        this.labelNumCol = labelNumCol;
    }

    public int getColunas() {
        return colunas;
    }

    public void setColunas(int colunas) {
        this.colunas = colunas;
    }

    public JLabel getNumRodape() {
        return numRodape;
    }

    public void setNumRodape(JLabel numRodape) {
        this.numRodape = numRodape;
    }

    public boolean isClicada() {
        return clicada;
    }

    public void setClicada(boolean clicada) {
        this.clicada = clicada;
    }

    public int getNumPrimeiraQuest() {
        return numPrimeiraQuest;
    }

    public void setNumPrimeiraQuest(int numPrimeiraQuest) {
        this.numPrimeiraQuest = numPrimeiraQuest;
    }

    public int getNumUltimaQuest() {
        return numUltimaQuest;
    }

    public void setNumUltimaQuest(int numUltimaQuest) {
        this.numUltimaQuest = numUltimaQuest;
    }
    
    public JLabel getImageApagar() {
        return imageApagar;
    }

    public void setImageApagar(JLabel imageApagar) {
        this.imageApagar = imageApagar;
    }
    
    
    public int getPagN() {
        return pagN;
    }
    
    public void setPagN(int pagN) {
        this.pagN = pagN;
    }
    
}
