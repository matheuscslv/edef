package PrincipalCelso;

import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class Alternativa extends JTextArea {
    private ImageIcon iconeApagarPeq = new ImageIcon(getClass().getResource("/Imagens/ApagarPeq.png"));
    private ImageIcon iconeApagarPeqAC = new ImageIcon(getClass().getResource("/Imagens/ApagarPeqAC.png"));
    private ImageIcon iconeNegrito = new ImageIcon(getClass().getResource("/Imagens/negrito.png"));
    private ImageIcon iconeNegritoAC = new ImageIcon(getClass().getResource("/Imagens/negritoAC.png"));
    
    private JLabel apag, negri;
    private int identidade, coluna;
    private String textoVerdadeiro;
    private boolean taNegri;
    
    public Alternativa(int num, int numColunas){
        if(numColunas == 1){
            this.setSize(525, 40);
        }else if(numColunas == 2){
            this.setSize(240, 40);
        }
        this.setLineWrap(true);
        this.setWrapStyleWord(true);
        //this.setBackground(Color.yellow);
        
        this.coluna = numColunas;
        this.identidade = num;
        this.setFont(new Font("TIMES_ROMAN", Font.PLAIN, 11));
        this.taNegri = false;
        this.textoVerdadeiro = "Exemplo de Enunciado:\n" +
                                "(a) Alternativa A\n" +
                                "(b) Alternativa B\n" +
                                "(c) Alternativa C\n" +
                                "(d) Alternativa D\n" +
                                "(e) Alternativa E";
        this.setText(textoVerdadeiro);
        this.setToolTipText("Alternativa");
        
        apag = new JLabel(iconeApagarPeq);
        apag.setBounds(this.getWidth() - iconeApagarPeq.getIconWidth() - 8,3,iconeApagarPeq.getIconWidth(), iconeApagarPeq.getIconHeight());
        apag.setVisible(false);
        this.add(apag);
        this.apag.setToolTipText("Fechar alternativa");
        
        negri = new JLabel(iconeNegrito);
        negri.setBounds(this.getWidth() - iconeNegrito.getIconWidth() - 23,2,iconeNegrito.getIconWidth(), iconeNegrito.getIconHeight());
        negri.setVisible(false);
        this.add(negri);
        this.negri.setToolTipText("Negrito");
    }

    public JLabel getNegri() {
        return negri;
    }

    public void setNegri(JLabel negri) {
        this.negri = negri;
    }

    public boolean isTaNegri() {
        return taNegri;
    }

    public void setTaNegri(boolean taNegri) {
        this.taNegri = taNegri;
    }

    public int getIdentidade() {
        return identidade;
    }

    public void setIdentidade(int identidade) {
        this.identidade = identidade;
    }

    public JLabel getApag() {
        return apag;
    }

    public void setApag(JLabel apag) {
        this.apag = apag;
    }
}
