package PrincipalCelso;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class ImagEsq extends JLabel{
    
    private ImageIcon iconeSconteudo = new ImageIcon(getClass().getResource("/Imagens/ImagemSconteudo.png"));
    private ImageIcon iconeBranco = new ImageIcon(getClass().getResource("/Imagens/Branco.png"));
    
    private Imagem imp;
    
    private int num, w, h;
    
    public ImagEsq(String alinha, boolean vaiTerSet){
        //this.setBounds(0,0,635, iconeSconteudo.getIconHeight());
        
        this.setIcon(aumentaImagem(iconeBranco, iconeSconteudo.getIconHeight()));
        
        imp = new Imagem(vaiTerSet);
        if(alinha.equals("ESQUERDA")){
            this.imp.setAlinhamento("ESQUERDA");
            imp.setBounds(0,0,iconeSconteudo.getIconWidth(), iconeSconteudo.getIconHeight());
        }else if(alinha.equals("CENTRO")){
            this.imp.setAlinhamento("CENTRO");
            imp.setBounds((iconeBranco.getIconWidth() - iconeSconteudo.getIconWidth())/2,0,iconeSconteudo.getIconWidth(), iconeSconteudo.getIconHeight());
        }else if(alinha.equals("DIREITA")){
            this.imp.setAlinhamento("DIREITA");
            imp.setBounds(iconeBranco.getIconWidth() - iconeSconteudo.getIconWidth() - 5,0,iconeSconteudo.getIconWidth(), iconeSconteudo.getIconHeight());
        }
        this.w = imp.getWidth();
        this.h = imp.getHeight();
        
        this.add(imp);
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public Imagem getImp() {
        return imp;
    }

    public void setImp(Imagem imp) {
        this.imp = imp;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
    
    public ImageIcon aumentaImagem(ImageIcon arquivo, int alt){
        //ImageIcon imagem = new ImageIcon(arquivo.getAbsolutePath());
        ImageIcon thumbnail = null;
        
        thumbnail = new ImageIcon(
        arquivo.getImage().getScaledInstance(arquivo.getIconWidth(), alt, Image.SCALE_SMOOTH));
        return thumbnail;
    }
}
