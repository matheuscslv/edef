package PrincipalCelso;

import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

public class Imagem extends JLabel{
    private ImageIcon iconeApagarPeq = new ImageIcon(getClass().getResource("/Imagens/ApagarPeq.png"));
    private ImageIcon iconeApagarPeqAC = new ImageIcon(getClass().getResource("/Imagens/ApagarPeqAC.png"));
    private ImageIcon iconeSconteudo = new ImageIcon(getClass().getResource("/Imagens/ImagemSconteudo.png"));
    private ImageIcon iconeSetaEsq = new ImageIcon(getClass().getResource("/Imagens/SetaEsq.png"));
    private ImageIcon iconeSetaEsqAC = new ImageIcon(getClass().getResource("/Imagens/SetaEsqAC.png"));
    private ImageIcon iconeSetaDir = new ImageIcon(getClass().getResource("/Imagens/SetaDir.png"));
    private ImageIcon iconeSetaDirAC = new ImageIcon(getClass().getResource("/Imagens/SetaDirAC.png"));
    private ImageIcon iconeSetaCen = new ImageIcon(getClass().getResource("/Imagens/SetaCen.png"));
    private ImageIcon iconeSetaCenAC = new ImageIcon(getClass().getResource("/Imagens/SetaCenAC.png"));
    private ImageIcon iconeBranco = new ImageIcon(getClass().getResource("/Imagens/Branco.png"));
    
    MouseListener mlI;
    
    private JLabel apag;
    private JLabel es,ce,di;

    private JButton outraImg;
    private int numero;
    private boolean clicado, vaiTer, entrou, some;
    private String file, alinhamento = null;
    
    public Imagem(boolean vaiTerSeta){
        this.setBounds(0,0,iconeSconteudo.getIconWidth(), iconeSconteudo.getIconHeight());
        //this.setBackground(Color.yellow);
        
        this.entrou = false;
        this.clicado = false;
        this.vaiTer = false;
        this.setIcon(iconeSconteudo);
        this.setToolTipText("Imagem");
        this.file = ""+iconeSconteudo.toString();
        this.file = this.file.substring(6,this.file.length()).replaceAll("[/]","\\\\"+"\\\\");
        
        if (vaiTerSeta) {
            this.vaiTer = true;
            this.es = new JLabel(iconeSetaEsq);
            this.es.setBounds(this.getIcon().getIconWidth() - iconeSetaEsq.getIconWidth() - 52,10,iconeSetaEsq.getIconWidth(), iconeSetaEsq.getIconHeight());
            this.es.setVisible(false);
            this.es.setToolTipText("Mover à esquerda");
            this.add(es);

            this.ce = new JLabel(iconeSetaCen);
            this.ce.setBounds(this.getIcon().getIconWidth() - iconeSetaCen.getIconWidth() - 37,10,iconeSetaCen.getIconWidth(), iconeSetaCen.getIconHeight());
            this.ce.setVisible(false);
            this.ce.setToolTipText("Mover ao centro");
            this.add(ce);

            this.di = new JLabel(iconeSetaDir);
            this.di.setBounds(this.getIcon().getIconWidth() - iconeSetaDir.getIconWidth() - 21,10,iconeSetaDir.getIconWidth(), iconeSetaDir.getIconHeight());
            this.di.setVisible(false);
            this.di.setToolTipText("Mover à direita");
            this.add(di);
        }
        
        this.apag = new JLabel(iconeApagarPeq);
        this.apag.setBounds(this.getWidth() - iconeApagarPeq.getIconWidth() - 7,10,iconeApagarPeq.getIconWidth(), iconeApagarPeq.getIconHeight());
        this.apag.setVisible(false);
        this.add(apag);
        this.apag.setToolTipText("Fechar imagem");
    }
    
    public void alinhar(String alinha, int numColu){
        
        if(alinha.equals("ESQUERDA")){
            alinhamento = "ESQUERDA";
            this.setLocation(0, 0);
        }else if(alinha.equals("CENTRO")){
            alinhamento = "CENTRO";
            if(numColu == 1){
                this.setLocation((525 - this.getIcon().getIconWidth())/2, 0);
            }else if(numColu == 2){
                this.setLocation((240 - this.getIcon().getIconWidth())/2, 0);
            }
        }else if(alinha.equals("DIREITA")){
            alinhamento = "DIREITA";
            if(numColu == 1){
                this.setLocation(525 - this.getIcon().getIconWidth(), 0);
            }else if(numColu == 2){
                this.setLocation(240 - this.getIcon().getIconWidth(), 0);
            }
        }
        
    }

    public boolean isSome() {
        return some;
    }

    public void setSome(boolean some) {
        this.some = some;
    }

    public boolean isEntrou() {
        return entrou;
    }

    public void setEntrou(boolean entrou) {
        this.entrou = entrou;
    }

    public boolean isVaiTer() {
        return vaiTer;
    }

    public void setVaiTer(boolean vaiTer) {
        this.vaiTer = vaiTer;
    }

    public String getAlinhamento() {
        return alinhamento;
    }

    public void setAlinhamento(String alinhamento) {
        this.alinhamento = alinhamento;
    }
    
    public JLabel getEs() {
        return es;
    }

    public void setEs(JLabel es) {
        this.es = es;
    }

    public JLabel getCe() {
        return ce;
    }

    public void setCe(JLabel ce) {
        this.ce = ce;
    }

    public JLabel getDi() {
        return di;
    }

    public void setDi(JLabel di) {
        this.di = di;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
    
    public boolean isClicado() {
        return clicado;
    }

    public void setClicado(boolean clicado) {
        this.clicado = clicado;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public JLabel getApag() {
        return apag;
    }

    public void setApag(JLabel apag) {
        this.apag = apag;
    }
}
