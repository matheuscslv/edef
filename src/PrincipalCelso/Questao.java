package PrincipalCelso;

import java.awt.Color;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

public class Questao extends JTextArea {
    private ImageIcon iconeApagarPeq = new ImageIcon(getClass().getResource("/Imagens/ApagarPeq.png"));
    private ImageIcon iconeApagarPeqAC = new ImageIcon(getClass().getResource("/Imagens/ApagarPeqAC.png"));
    private ImageIcon iconeNegrito = new ImageIcon(getClass().getResource("/Imagens/negrito.png"));
    private ImageIcon iconeNegritoAC = new ImageIcon(getClass().getResource("/Imagens/negritoAC.png"));
    
    private JLabel apag, negri;
    private int questN, colunas;
    private String textoVerdadeiro;
    private JTextField caixaDeNum;
    private boolean taNegri;
    
    public Questao(int numero, int numColunas){
        if(numColunas == 1){
            this.setSize(525, 40);
        }else if(numColunas == 2){
            this.setSize(240, 40);
        }
        this.setLineWrap(true);
        this.setWrapStyleWord(true);
        //this.setBackground(Color.yellow);
        
        this.colunas = numColunas;
        this.questN = numero;
        this.setFont(new Font("TIMES_ROMAN", Font.PLAIN, 11));
        this.taNegri = false;
        this.textoVerdadeiro = "Exemplo de Pre-enunciado";
        this.setText(textoVerdadeiro);
        this.setToolTipText("Questão: "+numero);
        
        apag = new JLabel(iconeApagarPeq);
        apag.setBounds(this.getWidth() - iconeApagarPeq.getIconWidth() - 8,3,iconeApagarPeq.getIconWidth(), iconeApagarPeq.getIconHeight());
        apag.setVisible(false);
        this.add(apag);
        this.apag.setToolTipText("Fechar questão");
        
        negri = new JLabel(iconeNegrito);
        negri.setBounds(this.getWidth() - iconeNegrito.getIconWidth() - 23,2,iconeNegrito.getIconWidth(), iconeNegrito.getIconHeight());
        negri.setVisible(false);
        this.add(negri);
        this.negri.setToolTipText("Negrito");
        
        caixaDeNum = new JTextField(" Questão: "+questN);
        caixaDeNum.setEditable(false);
        caixaDeNum.setBackground(Color.LIGHT_GRAY);
        caixaDeNum.setForeground(Color.WHITE);
        caixaDeNum.setFont(new Font("TIMES_ROMAN", Font.BOLD, 12));
        caixaDeNum.setBorder(new LineBorder(Color.black,1));
        caixaDeNum.setBounds(0, 0, 70, 15);
        this.add(caixaDeNum);
        caixaDeNum.setVisible(false);
        
    }
    public void mudarTexto(int n){
        this.caixaDeNum.setText(" Questão: "+n);
        this.setToolTipText("Questão: "+n);
        this.questN = n;
    }

    public boolean isTaNegri() {
        return taNegri;
    }

    public void setTaNegri(boolean taNegri) {
        this.taNegri = taNegri;
    }

    public JLabel getNegri() {
        return negri;
    }

    public void setNegri(JLabel negri) {
        this.negri = negri;
    }

    public int getColunas() {
        return colunas;
    }

    public void setColunas(int colunas) {
        this.colunas = colunas;
    }

    public JTextField getCaixaDeNum() {
        return caixaDeNum;
    }

    public void setCaixaDeNum(JTextField caixaDeNum) {
        this.caixaDeNum = caixaDeNum;
    }
    
    public int getQuestN() {
        return questN;
    }

    public void setQuestN(int questN) {
        this.questN = questN;
    }

    public JLabel getApag() {
        return apag;
    }

    public void setApag(JLabel apag) {
        this.apag = apag;
    }
    
    
}
