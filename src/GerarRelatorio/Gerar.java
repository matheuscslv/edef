package GerarRelatorio;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Gerar {
    
    private final String sql = "SELECT usuarios.nome,usuarios.matricula,usuario_disciplina.nota FROM usuarios \n" +
        "INNER JOIN usuario_disciplina ON usuarios.matricula = usuario_disciplina.usuarios_matricula \n" +
        "WHERE usuario_disciplina.disciplinas_codigo = ? order by usuarios.nome"; 
    
    private final String url = "jdbc:mysql://"+new Ip.Servidor().getIp()+":3306/edef";
    private final String username = new Ip.Servidor().getUsuario();
    private final String password = new Ip.Servidor().getSenha();
    
    public void gerar(String codigoDisciplina,String saida){
        
        ArrayList<String[]> alunoNota = new ArrayList<>();
        String nomeDaDisciplina = "";
                
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, codigoDisciplina);
            ResultSet resultado = ps.executeQuery();
            
            while(resultado.next()){
                String[] aluno = new String[2];
                aluno[0] = resultado.getString("nome");
                aluno[1] = resultado_CG_CGS(resultado.getString("nota"),c,resultado.getString("matricula"));
                alunoNota.add(aluno);
            }
            
            ps = c.prepareStatement("select nome from disciplinas where codigo = ?");
            ps.setString(1, codigoDisciplina);
            ResultSet nomeDisciplina = ps.executeQuery();
            
            while(nomeDisciplina.next()){
                nomeDaDisciplina = nomeDisciplina.getString("nome");
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
         
        try{
            preProcessPDF(alunoNota,nomeDaDisciplina,saida);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        } 
        
    }
    
    public void preProcessPDF(ArrayList<String[]> alunoNota,String disciplina,String saida) throws DocumentException, BadElementException, IOException {
        Document documentoPDF = new Document();

        PdfWriter.getInstance(documentoPDF, new FileOutputStream(saida+"\\"+disciplina+".pdf"));
        documentoPDF.open();
            
        Document pdf = documentoPDF;
        pdf.setPageSize(PageSize.A4);
        
        pdf.open();
        
        PdfPTable table = new PdfPTable(2);
        
        table.addCell(new PdfPCell(new Phrase("Aluno")));
        table.addCell(new PdfPCell(new Phrase("Nota")));

        for(int i=0;i<alunoNota.size();i++){
            table.addCell(new PdfPCell(new Phrase(alunoNota.get(i)[0])));
            table.addCell(new PdfPCell(new Phrase(alunoNota.get(i)[1])));
        }
        
        Font catFont = new Font(Font.FontFamily.TIMES_ROMAN,18, Font.BOLD);
        Paragraph p = new Paragraph("Resultado EDEF 2018.1 - "+disciplina, catFont);
        p.setAlignment(Element.ALIGN_CENTER);

        p.setSpacingAfter(20);

        pdf.add(p);
        pdf.add(table);
        pdf.close();

    }
    
    public String resultado_CG_CGS(String nota, Connection c, String matricula){
        try{
            double nota_double = Double.parseDouble(nota);
            String comando = "SELECT nota FROM usuario_disciplina WHERE disciplinas_codigo LIKE 'CCCG%' AND usuarios_matricula =  ?";
            PreparedStatement ps = c.prepareStatement(comando);
            ps.setString(1, matricula);
            ResultSet resultado = ps.executeQuery();
            
            double aux = Double.parseDouble(nota);
            while(resultado.next()){
                aux += Double.parseDouble(resultado.getString("nota"));
            }
            nota = ""+aux;
            
        }catch(Exception e){
            System.out.println("Erro em resultado_CG_CGS"+e);
        }
        return nota;
    }
}
