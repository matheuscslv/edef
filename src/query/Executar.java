package query;

import Principal.Capa;
import Principal.GerarProvaDuasColunas;
import Principal.MergePDF;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Executar {
    
    private final String url = "jdbc:mysql://"+new Ip.Servidor().getIp()+":3306/edef";
    private final String username = new Ip.Servidor().getUsuario();
    private final String password = new Ip.Servidor().getSenha();
    
    private ArrayList<String> provas = new ArrayList<>();
    private ArrayList<String> txts = new ArrayList<>();
    
    public void formatarDisciplina(File txtdisciplina,String codigo) throws SQLException{
        
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("UPDATE disciplinas set formatacao = ? WHERE codigo = ?");

            InputStream is = new FileInputStream(txtdisciplina);
            byte[] bytes = new byte[(int)txtdisciplina.length()];
            
            int offset = 0;
            int numRead = 0;
            while (offset < bytes.length && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
                offset += numRead;
            }

            ps.setBytes( 1, bytes );
            ps.setString( 2, codigo );       
            ps.execute();
            
            ps = c.prepareStatement("DELETE FROM questoes WHERE iddisciplina = ?");
            ps.setString( 1, codigo );
            ps.execute();
            
            ArrayList<Integer> alternativa = new PegarAlternativaCorreta().gerar(txtdisciplina.getAbsolutePath());
            for(int i=0;i<alternativa.size();i++){
                ps = c.prepareStatement("INSERT INTO questoes( iddisciplina, questao, correta) VALUES (?,?,?)");
                ps.setString( 1, codigo );
                ps.setInt( 2, (i+1) );
                ps.setInt( 3, alternativa.get(i) );
                
                ps.execute();
            }

            ps.close();
            c.close();
            
            JOptionPane.showMessageDialog(null,"Inserido com Sucesso");
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,ex.getMessage());
        }
    }
    
    public ArrayList<String> pegarDisciplinas() throws SQLException{
        ArrayList<String> discs = new ArrayList<String>();
        
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("SELECT * FROM disciplinas ");
            ResultSet disciplinas = ps.executeQuery();
                        
            while(disciplinas.next()){
                String codigo = disciplinas.getString("codigo");
                String nome = disciplinas.getString("nome");
                discs.add(codigo+"-"+nome);
            }
            
            ps.close();
            disciplinas.close();
            c.close();
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        
        return discs;
    }
    
    public int[] pegarGabarito(String matricula){
        int[] gabaritofinal = new int[45];
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("select questoes.correta as alternativa,questoes.iddisciplina from alunos inner join aluno_disciplina on aluno_disciplina.idaluno = alunos.matricula inner join disciplinas on aluno_disciplina.iddisciplina = disciplinas.codigo inner join questoes on questoes.iddisciplina = aluno_disciplina.iddisciplina where alunos.matricula = ? and disciplinas.codigo = \"CCCG\" UNION ALL select questoes.correta as alternativa,questoes.iddisciplina from alunos inner join aluno_disciplina on aluno_disciplina.idaluno = alunos.matricula inner join disciplinas on aluno_disciplina.iddisciplina = disciplinas.codigo inner join questoes on questoes.iddisciplina = aluno_disciplina.iddisciplina where alunos.matricula = ? and disciplinas.codigo <> \"CCCG\"");
            ps.setString(1, matricula);
            ps.setString(2, matricula);
            ResultSet gabarito = ps.executeQuery();
            
            int i = 0;
            while(gabarito.next()){                
                String valor = gabarito.getString("alternativa");
                gabaritofinal[i] = Integer.parseInt(valor);
                i++;
            }
                        
            ps.close();
            c.close();
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    
        return gabaritofinal;
    }
    
    public String[] pegarDisciplinasAlunos(String matricula){
        String[] gabaritoinicial = new String[10];
        String[] gabaritofinal = null;
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("select disciplinas.nome as nome,alunos.nome as aluno from alunos inner join aluno_disciplina on aluno_disciplina.idaluno = alunos.matricula inner join disciplinas on aluno_disciplina.iddisciplina = disciplinas.codigo where alunos.matricula = ?");
            ps.setString(1, matricula);
            ResultSet gabarito = ps.executeQuery();
            
            int i = 0;
            while(gabarito.next()){                
                String valor = gabarito.getString("nome");
                gabaritoinicial[i] = valor;
                i++;
            }
            
            gabaritofinal = new String[i];
            for(int j=0;j<gabaritofinal.length;j++){
                gabaritofinal[j] = gabaritoinicial[j];
            }
            
            ps.close();
            c.close();
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    
        return gabaritofinal;
    }
    
    public String[] pegarCodigoDisciplinasAlunos(String matricula){
        String[] gabaritoinicial = new String[10];
        String[] gabaritofinal = null;
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("select disciplinas.codigo as codigo,alunos.nome as aluno from alunos inner join aluno_disciplina on aluno_disciplina.idaluno = alunos.matricula inner join disciplinas on aluno_disciplina.iddisciplina = disciplinas.codigo where alunos.matricula = ? and disciplinas.codigo = \"CCCG\" UNION ALL select disciplinas.codigo as codigo,alunos.nome as aluno from alunos inner join aluno_disciplina on aluno_disciplina.idaluno = alunos.matricula inner join disciplinas on aluno_disciplina.iddisciplina = disciplinas.codigo where alunos.matricula = ? and disciplinas.codigo <> \"CCCG\"");
            ps.setString(1, matricula);
            ps.setString(2, matricula);
            ResultSet gabarito = ps.executeQuery();
            
            int i = 0;
            while(gabarito.next()){                
                String valor = gabarito.getString("codigo");
                gabaritoinicial[i] = valor;
                i++;
            }
            
            gabaritofinal = new String[i];
            for(int j=0;j<gabaritofinal.length;j++){
                gabaritofinal[j] = gabaritoinicial[j];
            }
            
            ps.close();
            c.close();
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    
        return gabaritofinal;
    }
    
    public int qtdDisciplinasQueAlunoFaz(String disciplina){
        int valor = 0;
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("select max(questoes.questao) as qtd from questoes where questoes.iddisciplina = ?");
            ps.setString(1, disciplina);
            ResultSet gabarito = ps.executeQuery();
            
            while(gabarito.next()){                
                valor = gabarito.getInt("qtd");
            }
                        
            ps.close();
            c.close();
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    
        return valor;
    }
    
    public void GerarProva(String saida) throws SQLException{
        
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("SELECT * FROM disciplinas WHERE formatacao IS NOT null");
            ResultSet disciplinastxt = ps.executeQuery();
            
            while(disciplinastxt.next()){
                String nome = disciplinastxt.getString("nome");
                byte [] bytes = disciplinastxt.getBytes("formatacao");
                File materia = new File(saida+"\\"+nome+".txt");
                FileOutputStream fos = new FileOutputStream(materia);
                fos.write( bytes );
                fos.close(); 
                                
                txts.add(saida+"\\"+nome+".txt");
            }

            ps = c.prepareStatement("SELECT * FROM alunos order by(nome)");
            ResultSet alunos = ps.executeQuery();

            while(alunos.next()){
                GerarProvaDuasColunas.numeracao = 3;
                
                Capa capa = new Capa();
                capa.gerar(saida, alunos.getString("nome").toUpperCase());
                
                String idaluno = alunos.getString("matricula");
                ps = c.prepareStatement("select disciplinas.codigo,disciplinas.nome,alunos.nome as aluno from alunos inner join aluno_disciplina on aluno_disciplina.idaluno = alunos.matricula inner join disciplinas on aluno_disciplina.iddisciplina = disciplinas.codigo where alunos.matricula = ? and disciplinas.codigo = \"CCCG\" UNION ALL select disciplinas.codigo,disciplinas.nome,alunos.nome as aluno from alunos inner join aluno_disciplina on aluno_disciplina.idaluno = alunos.matricula inner join disciplinas on aluno_disciplina.iddisciplina = disciplinas.codigo where alunos.matricula = ? and disciplinas.codigo <> \"CCCG\"");
                ps.setString(1, idaluno);
                ps.setString(2, idaluno);
                ResultSet disciplinas = ps.executeQuery();
                
                provas = new ArrayList<>();
                    
                ArrayList<String> temp = new ArrayList<String>();
                while(disciplinas.next()){
                    String nome = disciplinas.getString("nome").toUpperCase();
          
                    if(disciplinas.getString("codigo").equals("CCCG")){
                        GerarProvaDuasColunas prova = new GerarProvaDuasColunas();
                        provas.add(prova.gerar(saida+"\\"+nome+".txt", saida, nome));
                    }else{
                        GerarProvaDuasColunas prova = new GerarProvaDuasColunas();
                        temp.add(prova.gerar(saida+"\\"+nome+".txt", saida, nome));
                    }
                    
                }
                  
                for(int i=0;i<temp.size();i++){
                    provas.add(temp.get(i));
                }
                
                new MergePDF().saida(provas,saida,alunos.getString("nome"),alunos.getString("turma"));
                                                    
                System.out.println("Gerado com sucesso: "+alunos.getString("nome"));
                
                for(int i=0;i<provas.size();i++){
                    File excluir = new File(provas.get(i));
                    excluir.delete();
                }
                
            }
                                    
            for(int i=0;i<txts.size();i++){
                File excluir = new File(txts.get(i));
                excluir.delete();
            }
            
            ps.close();
            c.close();
            
            JOptionPane.showMessageDialog(null,"Gerado com Sucesso");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        
    }
    
    public void armazenarNota(String nome, String[] qtdDisciplinas, double[] notas){
        
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            PreparedStatement ps = null;
            
            for(int i=0;i<notas.length;i++){
                ps = c.prepareStatement("INSERT INTO notas(matricula, codigo, nota) VALUES (?,?,?)");
                ps.setString( 1, nome );
                ps.setString( 2, qtdDisciplinas[i] );
                ps.setString( 3, String.valueOf(notas[i]) );
                
                ps.execute();
            }
                       
            ps.close();
            c.close();
            
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,ex.getMessage());
        }
        
    }
    
    public void pegarNotas(){
        //select alunos.nome,disciplinas.nome as disciplina,notas.nota from notas inner join alunos on alunos.matricula = notas.matricula inner join disciplinas on disciplinas.codigo = notas.codigo;
    }
    
}
