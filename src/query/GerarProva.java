package query;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;


public class GerarProva {
    
    private final String url = "jdbc:mysql://"+new Ip.Servidor().getIp()+":3306/edef";
    private final String username = new Ip.Servidor().getUsuario();
    private final String password = new Ip.Servidor().getSenha();
    
    private ArrayList<String> provas = new ArrayList<>();
    private ArrayList<String> provaFinal = new ArrayList<>();
    private ArrayList<String> txts = new ArrayList<>();
    
    private int numeracao = 11;
    
    public void GerarProva(String saida,String periodo) throws SQLException{
        
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("SELECT * FROM disciplina_formatacao WHERE formatacao IS NOT null AND periodo = ?");
            ps.setString(1, periodo);
            ResultSet disciplinastxt = ps.executeQuery();
                        
            while(disciplinastxt.next()){
                
                byte [] bytes = disciplinastxt.getBytes("formatacao");
     		File materia = new File(saida+"\\"+disciplinastxt.getString("disciplinas_codigo")+".docx");
                FileOutputStream fos = new FileOutputStream(materia);
                fos.write( bytes );
                fos.close(); 
                                
                txts.add(saida+"\\"+disciplinastxt.getString("disciplinas_codigo")+".docx");
            }

            ps = c.prepareStatement("SELECT * FROM usuarios WHERE matricula = ? order by(nome)");
            ps.setString(1, "admin");
            ResultSet alunos = ps.executeQuery();
            
            while(alunos.next()){
                ps = c.prepareStatement("select usuario_disciplina.disciplinas_codigo, disciplinas.nome as disciplina,usuario_disciplina.usuarios_matricula, usuarios.nome as aluno \n" +
                                        "from usuario_disciplina inner join usuarios on usuario_disciplina.usuarios_matricula = usuarios.matricula\n" +
                                        "inner join disciplinas on disciplinas.codigo = usuario_disciplina.disciplinas_codigo");
                ResultSet disciplinasAlunos = ps.executeQuery();
                
                numeracao = 11;
                boolean cccg = false;
                while(disciplinasAlunos.next()){
                    if(disciplinasAlunos.getString("disciplinas_codigo").equals("CCCG")){
                        cccg = true;
                    }
                    find_replace_in_DOCX(saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+".docx",saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"Numerada.docx");
                    numeracao++;
                    provas.add(saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"Numerada.docx");
                    
                    converter(saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"Numerada.docx",saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"Numeradafinal.pdf");
                    String resposta = new RetiraPrimeiraPagina().saida(saida+"\\"+disciplinasAlunos.getString("disciplinas_codigo")+"Numeradafinal.pdf",saida,disciplinasAlunos.getString("disciplinas_codigo")+"Numeradafinalizada");
                    provaFinal.add(resposta);
                }
                
                if(cccg){
                    ArrayList<String> provasTemp = new ArrayList<>();
                    provasTemp.add(saida+"\\CCCGNumeradaFinalizada.pdf");
                    for(int i=0;i<provaFinal.size();i++){
                        provasTemp.add(provaFinal.get(i));
                    }
                    provaFinal = new ArrayList<>();
                    for(int i=0;i<provasTemp.size();i++){
                        provaFinal.add(provasTemp.get(i));
                    }
                }
                
                new MergePDF().saida(provaFinal, saida, alunos.getString("aluno"),alunos.getString("turma"));
                                
            }
                                                
            for(int i=0;i<txts.size();i++){
                File excluir = new File(txts.get(i));
                excluir.delete();
            }
            
            ps.close();
            c.close();
            
            JOptionPane.showMessageDialog(null,"Gerado com Sucesso");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        
    }
    
    public void find_replace_in_DOCX(String entrada, String saida) throws IOException,InvalidFormatException,org.apache.poi.openxml4j.exceptions.InvalidFormatException {
        
        try {
            XWPFDocument doc = new XWPFDocument(OPCPackage.open(entrada));
            for (XWPFParagraph p : doc.getParagraphs()) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null && text.contains("§")) {
                            text = text.replace("§", "QUESTÃO "+numeracao);
                            r.setText(text, 0);
                        }                      
                    }
                }
            }
            
            doc.write(new FileOutputStream(saida));
            
        }finally{}

    }
    
    public void converter(String docx,String pdf){
        com.aspose.words.Document doc;
        try {
            doc = new com.aspose.words.Document(docx);
            doc.save(pdf);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        File excluir = new File(docx);
        excluir.delete();
    }   
    
}
