package Principal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class GerarProvaUmaColuna {
    
    public static boolean chegouNoFinal = false;
    
    public String gerar(String saida,String nome,BufferedReader lerArq,String nomeAbsoluto) {
               
        try{
            XWPFDocument document = new XWPFDocument(new FileInputStream(System.getProperty("user.dir")+"/src/resources/ModeloCG_OBJ.docx"));
            String linha = lerArq.readLine();
                        
            XWPFParagraph numeroQuestao1 = document.createParagraph();
            XWPFRun numeroQuestaoTexto1 = numeroQuestao1.createRun();
            numeroQuestaoTexto1.setText("Questão "+GerarProvaDuasColunas.numeracao);
            numeroQuestaoTexto1.setBold(true);
                        
                while(linha != null) {
                    
                    if(linha.equals("$")){
                        linha = null;
                        
                        XWPFParagraph paragrafo = document.createParagraph();
                        XWPFRun texto = paragrafo.createRun();
                        texto.addBreak();

                        FileOutputStream fileOutput = new FileOutputStream(new File(saida+"\\ProvaUmaColuna.docx"));
                        document.write(fileOutput);
                        converter(saida+"\\ProvaUmaColuna.docx",saida+"\\"+nome+".pdf");
                        
                        ArrayList provas = new ArrayList<>();
                        provas.add(saida+"\\"+nome+".pdf");
                        String resposta = new RetiraPrimeiraPagina().saida(provas,saida,nome);

                        File excluir = new File(saida+"\\"+nome+".pdf");
                        excluir.delete();

                        return resposta;
                    }else{
                    
                    if(linha.substring(0,1).equals("'")){
                        if(linha.substring(1,2).equals("{")){
                            XWPFParagraph paragrafo = document.createParagraph();
                            XWPFRun texto = paragrafo.createRun();
                            
                            if(linha.substring(2,3).equals("|")){
                                if(linha.substring(3,4).equals("!")){
                                    paragrafo.setAlignment(ParagraphAlignment.CENTER);
                                    texto.setText(linha.substring(4, linha.length()-2));
                                    texto.setItalic(true);
                                }else{
                                    paragrafo.setAlignment(ParagraphAlignment.CENTER);
                                    texto.setText(linha.substring(3, linha.length()-2));
                                }
                            }else if(linha.substring(2,3).equals("/")){
                                if(linha.substring(3,4).equals("!")){
                                    paragrafo.setAlignment(ParagraphAlignment.RIGHT);
                                    texto.setText(linha.substring(4, linha.length()-2));
                                    texto.setItalic(true);
                                }else{
                                    paragrafo.setAlignment(ParagraphAlignment.RIGHT);
                                    texto.setText(linha.substring(3, linha.length()-2));
                                }
                            }else{
                                paragrafo.setAlignment(ParagraphAlignment.LEFT);
                                if(linha.substring(2,3).equals("!")){
                                    texto.setText(linha.substring(3, linha.length()-2));
                                    texto.setItalic(true);
                                }else{
                                    texto.setText(linha.substring(2, linha.length()-2));
                                }
                            }
                            
                            texto.setBold(true);
                                                                                    
                        }else{
                            XWPFParagraph paragrafo = document.createParagraph();
                            XWPFRun texto = paragrafo.createRun();
                            
                            if(linha.substring(1,2).equals("|")){
                                if(linha.substring(2,3).equals("!")){
                                    paragrafo.setAlignment(ParagraphAlignment.CENTER);
                                    texto.setText(linha.substring(3, linha.length()-1));
                                    texto.setItalic(true);
                                }else{
                                    paragrafo.setAlignment(ParagraphAlignment.CENTER);
                                    texto.setText(linha.substring(2, linha.length()-1));
                                }
                            }else if(linha.substring(1,2).equals("/")){
                                if(linha.substring(2,3).equals("!")){
                                    paragrafo.setAlignment(ParagraphAlignment.RIGHT);
                                    texto.setText(linha.substring(3, linha.length()-1));
                                    texto.setItalic(true);
                                }else{
                                    paragrafo.setAlignment(ParagraphAlignment.RIGHT);
                                    texto.setText(linha.substring(2, linha.length()-1));
                                }
                            }else{
                                paragrafo.setAlignment(ParagraphAlignment.LEFT);
                                if(linha.substring(1,2).equals("!")){
                                    texto.setText(linha.substring(2, linha.length()-1));
                                    texto.setItalic(true);
                                }else{
                                    texto.setText(linha.substring(1, linha.length()-1));
                                }
                            }
                            
                        }
                        
                        linha = lerArq.readLine();
                    }else{
                        if(linha.equals("-")){
                            GerarProvaDuasColunas.numeracao++;
                            linha = lerArq.readLine();
                            linha = lerArq.readLine();
                            
                            if(linha != null && !linha.equals("$")){

                                XWPFParagraph numeroQuestao = document.createParagraph();
                                XWPFRun numeroQuestaoTexto = numeroQuestao.createRun();
                                numeroQuestaoTexto.addBreak();
                                numeroQuestaoTexto.setText("Questão "+GerarProvaDuasColunas.numeracao);
                                numeroQuestaoTexto.setBold(true);
                                      
                            }
                            
                        }else{
                            if(linha.substring(0, 2).equals("--") && !linha.substring(0, 3).equals("---")){
                                int largura = 0,altura = 0;
                                int indiceLargura = 2;
                                while(!linha.substring(indiceLargura, indiceLargura+1).equals("W")){
                                    indiceLargura++;
                                }

                                largura = Integer.parseInt(linha.substring(2, indiceLargura));
                                
                                int indiceAltura = indiceLargura;
                                while(!linha.substring(indiceAltura, indiceAltura+1).equals("H")){
                                    indiceAltura++;
                                }
                                altura = Integer.parseInt(linha.substring(indiceLargura+1, indiceAltura));
                                
                                InputStream is = new FileInputStream(linha.substring(indiceAltura+1, linha.length())); 
                                XWPFParagraph imagens = document.createParagraph();
                                imagens.setAlignment(ParagraphAlignment.LEFT);
                                XWPFRun img = imagens.createRun();
                                img.addPicture(is, Document.PICTURE_TYPE_JPEG, "", Units.toEMU(largura), Units.toEMU(altura));
                                
                                linha = lerArq.readLine();
                            }else if(linha.substring(0, 3).equals("---")){
                                int largura = 0,altura = 0;
                                int indiceLargura = 3;
                                while(!linha.substring(indiceLargura, indiceLargura+1).equals("W")){
                                    indiceLargura++;
                                }

                                largura = Integer.parseInt(linha.substring(3, indiceLargura));
                                
                                int indiceAltura = indiceLargura;
                                while(!linha.substring(indiceAltura, indiceAltura+1).equals("H")){
                                    indiceAltura++;
                                }
                                altura = Integer.parseInt(linha.substring(indiceLargura+1, indiceAltura));
                                                                
                                InputStream is = new FileInputStream(linha.substring(indiceAltura+1, linha.length())); 
                                XWPFParagraph imagens = document.createParagraph();
                                imagens.setAlignment(ParagraphAlignment.RIGHT);
                                XWPFRun img = imagens.createRun();
                                img.addPicture(is, Document.PICTURE_TYPE_JPEG, "", Units.toEMU(largura), Units.toEMU(altura));
                                
                                linha = lerArq.readLine();
                            }else{
                                
                                int largura = 0,altura = 0;
                                int indiceLargura = 0;
                                while(!linha.substring(indiceLargura, indiceLargura+1).equals("W")){
                                    indiceLargura++;
                                }

                                largura = Integer.parseInt(linha.substring(0, indiceLargura));
                                
                                int indiceAltura = indiceLargura;
                                while(!linha.substring(indiceAltura, indiceAltura+1).equals("H")){
                                    indiceAltura++;
                                }
                                altura = Integer.parseInt(linha.substring(indiceLargura+1, indiceAltura));
                                
                                
                                InputStream is = new FileInputStream(linha.substring(indiceAltura+1, linha.length())); 
                                XWPFParagraph imagens = document.createParagraph();
                                imagens.setAlignment(ParagraphAlignment.CENTER);
                                XWPFRun img = imagens.createRun();
                                img.addPicture(is, Document.PICTURE_TYPE_JPEG, "", Units.toEMU(largura), Units.toEMU(altura));
                                
                                linha = lerArq.readLine();
                            }
                        }
                    }
                }
            }
                                        
            XWPFParagraph paragrafo = document.createParagraph();
            XWPFRun texto = paragrafo.createRun();
            texto.addBreak();
            
            FileOutputStream fileOutput = new FileOutputStream(new File(saida+"\\ProvaUmaColuna.docx"));
            document.write(fileOutput);
               
            converter(saida+"\\ProvaUmaColuna.docx",saida+"\\"+nome+".pdf");
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        
        ArrayList provas = new ArrayList<>();
        provas.add(saida+"\\"+nome+".pdf");
        String resposta = new RetiraPrimeiraPagina().saida(provas,saida,nome);
        
        File excluir = new File(saida+"\\"+nome+".pdf");
        excluir.delete();
            
        chegouNoFinal = true;
        
        return resposta;
               
    }

    public void converter(String docx,String pdf){
        com.aspose.words.Document doc;
        try {
            doc = new com.aspose.words.Document(docx);
            doc.save(pdf);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        File excluir = new File(docx);
        excluir.delete();
    }   
    
}
