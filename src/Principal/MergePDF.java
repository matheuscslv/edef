package Principal;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MergePDF {

    public void saida(ArrayList<String> arquivos,String saida,String aluno,String grupo) {
        try {
            List<InputStream> pdfs = new ArrayList<InputStream>();
            boolean success = (new File(saida+"\\"+grupo)).mkdir();
            pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/inicial.pdf"));
            
            for(int i=0;i<arquivos.size();i++){
                pdfs.add(new FileInputStream(arquivos.get(i)));
            }
            
            pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/final.pdf"));
            
            if(GerarProvaDuasColunas.precisa_mais){
                pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/cartao_maior_40.pdf"));
                GerarProvaDuasColunas.precisa_mais = false;
            }
            
            OutputStream output = new FileOutputStream(saida+"\\"+grupo+"\\"+aluno+".pdf");
            MergePDF.concatPDFs(pdfs, output, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void saida(ArrayList<String> arquivos,String saida,String nome) {
        try {
            List<InputStream> pdfs = new ArrayList<InputStream>();

            for(int i=0;i<arquivos.size();i++){
                pdfs.add(new FileInputStream(arquivos.get(i)));
            }
            
            OutputStream output = new FileOutputStream(saida+"\\"+nome+".pdf");
            MergePDF.concatPDFs(pdfs, output, true);
            
            for(int i=0;i<arquivos.size();i++){
                File excluir = new File(arquivos.get(i));
                excluir.delete();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void concatPDFs(List<InputStream> streamOfPDFFiles,
        OutputStream outputStream, boolean paginate) {
        Document document = new Document();
        try {
            List<InputStream> pdfs = streamOfPDFFiles;
            List<PdfReader> readers = new ArrayList<PdfReader>();
            int totalPages = 0;
            Iterator<InputStream> iteratorPDFs = pdfs.iterator();
            // Create Readers for the pdfs.
            while (iteratorPDFs.hasNext()) {
                InputStream pdf = iteratorPDFs.next();
                PdfReader pdfReader = new PdfReader(pdf);
                readers.add(pdfReader);
                totalPages += pdfReader.getNumberOfPages();
            }
            // Create a writer for the outputstream
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);
            document.open();
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA,
                    BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            PdfContentByte cb = writer.getDirectContent(); // Holds the PDF
            // data
            PdfImportedPage page;
            int currentPageNumber = 0;
            int pageOfCurrentReaderPDF = 0;//Aqui e essencial
            Iterator<PdfReader> iteratorPDFReader = readers.iterator();
            // Loop through the PDF files and add to the output.
            while (iteratorPDFReader.hasNext()) {
                PdfReader pdfReader = iteratorPDFReader.next();
                // Create a new page in the target for each source page.
                while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {
                    document.newPage();
                    pageOfCurrentReaderPDF++;
                    currentPageNumber++;
                    page = writer.getImportedPage(pdfReader,
                            pageOfCurrentReaderPDF);
                    cb.addTemplate(page, 0, 0);
                    // Code for pagination.
                    if (paginate) {
                        /*
                        cb.beginText();
                        cb.setFontAndSize(bf, 9);
                        cb.showTextAligned(PdfContentByte.ALIGN_CENTER, ""
                                + currentPageNumber + " of " + totalPages, 520,
                                5, 0);
                        cb.endText();
                        */
                    }
                }
                pageOfCurrentReaderPDF = 0;
            }
            outputStream.flush();
            document.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (document.isOpen())
                document.close();
            try {
                if (outputStream != null)
                    outputStream.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }            
        }
    }
}
