package Principal;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class GerarUmaColunaPDF {
    
    public String gerar(String disciplina,String saida,String nome) {
        
        Document documentoPDF = new Document(PageSize.A4,30,30,30,30);
        
        try{
            PdfWriter.getInstance(documentoPDF, new FileOutputStream(saida+"\\"+nome+".pdf"));
            documentoPDF.open();
            //documentoPDF.setPageSize(PageSize.A4);
            
            Font fonteTexto = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
            Font normal = new Font(Font.FontFamily.TIMES_ROMAN, 11);
            
            Image cabecalho = Image.getInstance(System.getProperty("user.dir")+"/src/resources/cabecalho.jpg");
            cabecalho.scalePercent(25, 22);
            cabecalho.setAlignment(Element.ALIGN_CENTER);
            documentoPDF.add(cabecalho);
            
            Paragraph nomeDisciplina = new Paragraph(nome,fonteTexto);
            nomeDisciplina.setAlignment(Element.ALIGN_CENTER);
            nomeDisciplina.setSpacingAfter(20);
            documentoPDF.add(nomeDisciplina);
            
            try {
                File arq = new File(disciplina);
                BufferedReader lerArq = new BufferedReader(new InputStreamReader(new FileInputStream(arq), "ISO-8859-1"));
                String linha = lerArq.readLine();
                while (linha != null) {
                    Paragraph paragrafo = new Paragraph();
                    paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
                                       
                    String primeiro = linha.substring(0,1);
                    if(primeiro.equals("'")){
                        int indice = 2;
                        if(linha.substring(1,2).equals("{")){
                            while(linha.charAt(indice) != '}'){
                                indice++;
                            }
                            indice++;
                            paragrafo.add(new Phrase(linha.substring(2,indice-1),fonteTexto));
                        }
                        if(indice == 2){
                            paragrafo.add(new Phrase(linha.substring(indice-1,linha.length()-1),normal));
                            documentoPDF.add(paragrafo);
                        }else{
                            paragrafo.add(new Phrase(linha.substring(indice,linha.length()-1),normal));
                            documentoPDF.add(paragrafo);
                        }
                    }else{
                        if(linha.equals("-")){
                            paragrafo.setSpacingAfter(20);
                            documentoPDF.add(paragrafo);
                        }else{
                            if(linha.substring(0, 2).equals("--")){
                                Image imagem = Image.getInstance(linha.substring(2, linha.length()));
                                imagem.scalePercent(30, 30);
                                imagem.setAlignment(Element.ALIGN_LEFT);
                                documentoPDF.add(imagem);
                            }else{
                                Image imagem = Image.getInstance(linha);
                                imagem.scalePercent(30, 30);
                                imagem.setAlignment(Element.ALIGN_CENTER);
                                documentoPDF.add(imagem);
                            }
                        }
                    }
                    
                    linha = lerArq.readLine();
                }
                lerArq.close();
            } catch (Exception e) {
                System.err.printf("Erro na abertura do arquivo: %s.\n",e.getMessage());
                e.printStackTrace();
            }
                        
        }catch(Exception e){
            System.out.println(e);
        }finally{
            documentoPDF.close();
        }
        
        return saida+"\\"+nome+".pdf";
        
    }
    
}
