package Corretor;

import PrincipalEduardo.Conexao_BD;
import static PrincipalEduardo.Janela.janela0;
import java.io.File;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class Insere_Notas_CCCGS {
    
    public static void main(String[] args){
        
        Workbook workbook = null;
        try{
            //Seleciona o arquivo excel e cria um WorkBook dele
            JFileChooser chooser = new JFileChooser();
            chooser.setFileFilter(new FileNameExtensionFilter("xlx","xlsx"));
            chooser.showOpenDialog(null);
            if(chooser.getSelectedFile() == null) return;
            workbook = WorkbookFactory.create(new File(chooser.getSelectedFile().getAbsolutePath()));
            
        }catch(Exception e){
            System.out.println("Erro em pegar o excel");
            JOptionPane.showMessageDialog(null, "O arquivo Excel não pode ser selecionado!\n(Verifique se o arquivo está aberto e feche-o)", "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        try{
            // Loop por todos os sheets do arquivo
            for(Sheet sheet: workbook) {
                
                pegaDados(sheet);
            }
        }catch(Exception e){
            System.out.println("Erro em verificaFormatacao: "+e);
            JOptionPane.showMessageDialog(null, "O arquivo não está formatado corretamente!");
            return;
        }
        
        try{
            workbook.close();
        }catch(Exception e){
            System.out.println("Erro ao fechar o Workbook: "+e);
            JOptionPane.showMessageDialog(null, "Um erro inesperado ocorreu. Tente novamente.");
            return;
        }
        
        
    }
    
    public static void pegaDados(Sheet sheet) {
        try{
            //Banco
            Connection conexao = new Conexao_BD().VerificarConexao(janela0);
            String comando = "update usuario_disciplina set nota = ? where usuarios_matricula = ? AND usuario_disciplina.disciplinas_codigo = 'CCCGS'";
            PreparedStatement ps = null;
            
            // Loop pra pegar todos os dados, tranformar em String e mandar pro "dados"
            DataFormatter dataFormatter = new DataFormatter();
            for(int i=2; i>=0; i++){
                Row row = sheet.getRow(i);
                Cell cell_1 = row.getCell(1), cell_2 = row.getCell(2);
                
                if(dataFormatter.formatCellValue(cell_1).equals("")) return;
                    
                String matricula = dataFormatter.formatCellValue(cell_1);
                String nota = dataFormatter.formatCellValue(cell_2);
                
                //Banco
                
                ps = conexao.prepareStatement(comando);
                ps.setString(1,nota);
                ps.setString(2,matricula);
                ps.executeUpdate();

                
            }
            
        }catch(Exception e){
            System.out.println("Erro dentro do pegaDados: "+e);
        }
    }
}
