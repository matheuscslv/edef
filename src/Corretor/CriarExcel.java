package Corretor;
 
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
 
public class CriarExcel {
        
    private final String url = "jdbc:mysql://"+new Ip.Servidor().getIp()+":3306/edef";
    private final String username = new Ip.Servidor().getUsuario();
    private final String password = new Ip.Servidor().getSenha();
    
    private static int linha = 1;
        
    public void gerarExcel(String saida){
        
        try{
            File excluir = new File(saida+"//notas.xls");
            excluir.delete();
            gerar("",saida);
        }catch(Exception e){}
        
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("select distinct(usuario_disciplina.usuarios_matricula),usuarios.nome from usuario_disciplina \n" +
                "inner join usuarios on usuario_disciplina.usuarios_matricula = usuarios.matricula order by usuarios.nome;");
            ResultSet disciplinas = ps.executeQuery();
                        
            while(disciplinas.next()){
                gerar(disciplinas.getString("usuarios_matricula"),saida);
            }
            
            ps.close();
            c.close();
            
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            ex.printStackTrace();
        }
        
    }
    
    public void gerar(String nomeAluno, String saida) {
        linha = 1;
        
        Workbook planilha = null;
        WritableSheet Atemp = null;
        WritableWorkbook Ptemp = null;
        try {
            planilha = Workbook.getWorkbook(new File(saida+"//notas.xls"));
            Ptemp = Workbook.createWorkbook(new File(saida+"//temp.xls"));
            Atemp = Ptemp.createSheet("ListaAlunos", 0);
            
            try {

                Sheet aba = planilha.getSheet(0);
                linha = aba.getRows();
                for (int i = 0; i < aba.getRows(); i++) {
                    Cell[] oi = aba.getRow(i);
                    for(int j=0;j<oi.length;j++){
                        Label label = new Label(j, i, oi[j].getContents());
                        Atemp.addCell(label);
                    }     
                }

                ArrayList<String[]> disciplinas = disciplinasAlunoAndNota(nomeAluno);
                
                for(int i=0;i<disciplinas.size();i++){
                    System.out.println(disciplinas.get(i)[0]+" "+disciplinas.get(i)[1]);
                }
                
                Label label = new Label(0, linha, nomeAluno(nomeAluno));
                Atemp.addCell(label);
                
                label = new Label(1, linha, nomeAluno);
                Atemp.addCell(label);
                
                label = new Label(2, linha, "");
                Atemp.addCell(label);
                
                label = new Label(3, linha, disciplinas.get(0)[1]);
                Atemp.addCell(label);
                
                for(int i=1;i<disciplinas.size();i++){
                    if(disciplinas.get(i)[0].equals("Programação I (Prof. Allan David)")){
                        label = new Label(4, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("Programação I (Prof. Eonay Gurjão)")){
                        label = new Label(5, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("CALCULO I")){
                        label = new Label(6, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("PROGRAMAÇÃO III")){
                        label = new Label(7, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("ÁLGEBRA LINEAR E GEOMETRIA ANALITICA")){
                        label = new Label(8, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("INTELIGÊNCIA ARTIFICIAL")){
                        label = new Label(9, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("BANCO DE DADOS I")){
                        label = new Label(10, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("REDE DE COMPUTADORES II")){
                        label = new Label(11, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("PROJETO E ANÁLISE DE ALGORITMO")){
                        label = new Label(12, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("TÓPICOS EM COMPUTAÇÃO MÓVEL E SEM FIO")){
                        label = new Label(13, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("FISICA I")){
                        label = new Label(14, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("Organização de Computadores (Prof. Eonay)")){
                        label = new Label(15, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("Organização de Computadores (Prof. Marco Leal)")){
                        label = new Label(16, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("INGLÊS INSTRUMENTAL")){
                        label = new Label(17, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("CÁLCULO III")){
                        label = new Label(18, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("ELETRÔNICA DIGITAL")){
                        label = new Label(19, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("METODOLOGIA E PESQUISA DO TRABALHO CIENTIFICO")){
                        label = new Label(20, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("ENGENHARIA DE SOFTWARE I")){
                        label = new Label(21, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("COMPILADORES")){
                        label = new Label(22, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("TEORIA DOS GRAFOS")){
                        label = new Label(23, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("COMPUTAÇÃO GRÁFICA")){
                        label = new Label(24, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("CÁLCULO NUMÉRICO")){
                        label = new Label(25, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("INTERAÇÃO HOMEM-MÁQUINA")){
                        label = new Label(26, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("TÓPICOS EM QUALIDADE DE SOFTWARE")){
                        label = new Label(27, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }else if(disciplinas.get(i)[0].equals("AUTÔMATOS E LINGUAGENS FORMAIS")){
                        label = new Label(28, linha, disciplinas.get(i)[1]);
                        Atemp.addCell(label);
                    }
                    
                }
                
                Label label2 = new Label(2, 0, "CONHECIMENTOS GERAIS");
                Atemp.addCell(label2);
                label2 = new Label(4, 0, "CONHECIMENTOS ESPECIFICOS");
                Atemp.addCell(label2);
                                
                Atemp.mergeCells(2, 0, 3, 0);
                Atemp.mergeCells(4, 0, disciplinas.size()+2, 0);
                
                Ptemp.write();               
                Ptemp.close();
                planilha.close();
                
                File excluir = new File(saida+"//notas.xls");
                excluir.delete();
                File renomear = new File(saida+"//temp.xls");
                renomear.renameTo(excluir);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception ex) {
            try{
                WritableWorkbook planilha2 = Workbook.createWorkbook(new File(saida+"//notas.xls"));
                WritableSheet aba2 = planilha2.createSheet("ListaAlunos", 0);

                ArrayList<String> disciplinas = disciplinas();
                                
                String cabecalho[] = new String[disciplinas.size()+3];
                cabecalho[0] = "ALUNOS";
                cabecalho[1] = "MATRICULA";
                cabecalho[2] = "CGS";
                cabecalho[3] = "CGO";
                for(int i=4;i<cabecalho.length;i++){
                    cabecalho[i] = disciplinas.get(i-3);
                }

                for (int i = 0; i < cabecalho.length; i++) {
                    Label label = new Label(i, 1, cabecalho[i]);
                    //aba2.setColumnView(i, cabecalho[i].length());
                    aba2.addCell(label);
                    WritableCell cell = aba2.getWritableCell(i, 0);
                }
                 
                Label label2 = new Label(2, 0, "CONHECIMENTOS GERAIS");
                aba2.addCell(label2);
                label2 = new Label(4, 0, "CONHECIMENTOS ESPECIFICOS");
                aba2.addCell(label2);

                aba2.mergeCells(2, 0, 3, 0);
                aba2.mergeCells(4, 0, disciplinas.size()+2, 0);
                
                planilha2.write();
                planilha2.close();
            }catch(Exception e){
                
            }
        }
                                  
    }
    
    public String nomeAluno(String matricula){
        String nome = "";
        
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("SELECT nome FROM usuarios WHERE matricula = ?");
            ps.setString(1, matricula);
            ResultSet disciplinas = ps.executeQuery();
                        
            while(disciplinas.next()){
                nome = disciplinas.getString("nome");
            }
            
            ps.close();
            c.close();
            
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            ex.printStackTrace();
        }
        
        return nome;
    }
    
    public ArrayList<String[]> disciplinasAlunoAndNota(String aluno){
        ArrayList<String[]> disc = new ArrayList<>();
        
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("SELECT usuarios.nome AS aluno,disciplinas.nome AS disciplina,usuario_disciplina.nota FROM usuario_disciplina \n" +
                "INNER JOIN disciplinas ON disciplinas.codigo = usuario_disciplina.disciplinas_codigo \n" +
                "INNER JOIN usuarios ON usuarios.matricula = usuario_disciplina.usuarios_matricula WHERE usuarios.matricula = ? \n" +
                "and usuario_disciplina.disciplinas_codigo != 'CCCGS' ORDER BY disciplinas.ordem");
            ps.setString(1, aluno);
            ResultSet disciplinas = ps.executeQuery();
                        
            while(disciplinas.next()){
                String[] nota = new String[2];
                nota[0] = disciplinas.getString("disciplina");
                nota[1] = disciplinas.getString("nota");
                disc.add(nota);
            }
            
            ps.close();
            c.close();
            
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            ex.printStackTrace();
        }
        
        return disc;
    }
    
    public ArrayList<String> disciplinas(){
        
        ArrayList<String> disc = new ArrayList<>();
        
        try {
            Connection c = DriverManager.getConnection(url, username, password);
            
            PreparedStatement ps = c.prepareStatement("SELECT DISTINCT(usuario_disciplina.disciplinas_codigo),disciplinas.nome FROM usuario_disciplina INNER JOIN disciplinas ON usuario_disciplina.disciplinas_codigo = disciplinas.codigo WHERE usuario_disciplina.disciplinas_codigo != 'CCCGS' ORDER BY disciplinas.ordem");
            ResultSet disciplinas = ps.executeQuery();
                        
            while(disciplinas.next()){
                disc.add(disciplinas.getString("nome"));
            }
            
            ps.close();
            c.close();
            
            
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            ex.printStackTrace();
        }
        
        return disc;
        
    } 
    
}