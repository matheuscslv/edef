package Telas;

import PrincipalCelso.Main;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import query.Executar;

public class Disciplinas {
    
    public static JList area;
    JFrame janela = new JFrame();
    public void gerar(){
        
        
        janela.setTitle("Disciplinas");
        janela.setSize(500,400);
        janela.setResizable(false);
        janela.setLayout(null);
        janela.setLocationRelativeTo(null);
        //janela.setIconImage(new ImageIcon(getClass().getResource("/resources/icon.png")).getImage());
        janela.addWindowListener( new WindowAdapter( ){
            public void windowClosing(WindowEvent w){
                janela.dispose();
            }
        });
        
        iniciarComponentes(janela);
        
        janela.setVisible(true);
        
    }
    
    public void iniciarComponentes(JFrame janela){
        JLabel lnome = new JLabel("Codigo da Disciplina: ");
        lnome.setBounds(10,10,200,20);
        JTextField nome = new JTextField();
        nome.setBounds(10,30,200,20);
                
        JButton disciplina = new JButton("Formatar Disciplina");
        disciplina.setBounds(10,330,200,30);
                
        JButton salvar = new JButton("Salvar");
        salvar.setBounds(10,180,200,30);
        
        DefaultListModel model = new DefaultListModel();
        area = new JList(model);
        
        try {
            ArrayList<String> nomes = new Executar().pegarDisciplinas();
            for(int i=0;i<nomes.size();i++){
                model.add(i, nomes.get(i));        
            }
        } catch (SQLException ex) {
            Logger.getLogger(Disciplinas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JScrollPane jscroll = new JScrollPane(area);          
        jscroll.setBounds(50,10,400,300);
        
        final JFileChooser materia = new JFileChooser();
        materia.setFileFilter(new FileNameExtensionFilter("Arquivos", "txt"));
        materia.setMultiSelectionEnabled(true);
                        
        //janela.add(lnome);
        //janela.add(nome);
        janela.add(disciplina);
        //janela.add(salvar);
        janela.add(jscroll);
        
        disciplina.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                
                try {
                    //java.awt.Desktop.getDesktop().open( new File(System.getProperty("user.dir")+"/GerarProva.jar") );
                    String valor = JOptionPane.showInputDialog(null, "Digite a Quantidade De Questões");
                    
                    if(Integer.parseInt(valor) <= 0 || Integer.parseInt(valor) > 100){
                        JOptionPane.showMessageDialog(null, "Fora do limite (1 à 100)");
                    }else{
                        Main iniciar = new Main();
                        iniciar.comecar(Integer.parseInt(valor));
                    }
   
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Digite um número adequado.");
                }

            }
        });
                
        salvar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                Executar executar = new Executar();
                try {
                    String codigo = area.getSelectedValue().toString();
                    String[] cod = codigo.split("-");                
                    executar.formatarDisciplina(new File(System.getProperty("user.dir")+"/Necessario/DadosDeDisciplinas.txt"),cod[0]);
                } catch (SQLException ex) {
                    Logger.getLogger(Disciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }                
            }
        });
        
    }
        
}
