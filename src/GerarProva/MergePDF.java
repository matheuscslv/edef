package GerarProva;

import PrincipalEduardo.Conexao_BD;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JFrame;

public class MergePDF {

    private ArrayList<String> provaOrdem = new ArrayList<>();
    
    static Connection conexao = null;
    static PreparedStatement ps = null;
    static ResultSet rs = null;
    int provas_1º_dia = 0, provas_2º_dia = 0;
    
    public void saida(ArrayList<String> arquivos,String saida,String aluno,String grupo) {
        try {
            List<InputStream> pdfs = new ArrayList<InputStream>();
            boolean success = (new File(saida+"\\"+grupo)).mkdir();
            
            //capa 1º dia
            pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/inicial.pdf"));
            
            //Verifica conexão
            conexao = new Conexao_BD().VerificarConexao(new JFrame());
            if(conexao == null) return;
            
            //Pega o codigo e a ordem para o 1º dia
            String frase = "select * from disciplinas where ordem > 0 and ordem < 12 order by ordem";
            ps = conexao.prepareStatement(frase);
            rs = ps.executeQuery();
            while(rs.next()){
                
                //Pega o nome e ordem da disciplina
                String codigo = rs.getObject("codigo").toString();
                int ordem = Integer.parseInt(rs.getObject("ordem").toString());
                
                //Loop nas disciplinas, verifica se pertence
                for(int i=0;i<arquivos.size();i++){
                    
                    String arquivo = codigoDisciplina(arquivos.get(i));
                    if(arquivo.equals(codigo)){
                        pdfs.add(new FileInputStream(arquivos.get(i)));
                        provas_1º_dia++;
                    }
                }
            
            }
            
            //rascunho 1º dia
            pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/rascunho_primeiro_dia.pdf"));
            if(provas_1º_dia > 6) // 6 disciplinas = 40 questões
                pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/rascunho_40_mais.pdf"));
            
            //Gabarito
            pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/discursivas.pdf"));
            pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/gabarito.pdf"));
            if(provas_1º_dia > 6) // 6 disciplinas = 40 questões
                pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/cartao_maior_40.pdf"));
            
            
            //capa 2º dia
            pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/inicial.pdf"));
            
            //Pega o codigo e a ordem para o 2º dia
            frase = "select * from disciplinas where ordem >= 12 order by ordem";
            ps = conexao.prepareStatement(frase);
            rs = ps.executeQuery();
            while(rs.next()){
                //Pega o nome e ordem da disciplina
                String codigo = rs.getObject("codigo").toString();
                int ordem = Integer.parseInt(rs.getObject("ordem").toString());
                
                //Loop nas disciplinas, verifica se pertence
                for(int i=0;i<arquivos.size();i++){
                    String arquivo = codigoDisciplina(arquivos.get(i));
                    if(arquivo.equals(codigo)){
                        pdfs.add(new FileInputStream(arquivos.get(i)));
                        provas_2º_dia++;
                    }
                }
            }
            
            

            if(provas_1º_dia > 6){ // > 40 questões
                pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/rascunho_40_mais.pdf"));
            }
            
            if(provas_1º_dia == 6){ // = 40 questões
                pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/rascunho_40_mais.pdf"));
                pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/cartao_maior_40.pdf"));
            }
            
            if(provas_1º_dia < 6){ // < 40 questões
                pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/rascunho_segundo_dia.pdf"));
                
                if((provas_1º_dia + provas_2º_dia ) > 6){ // 1ºdia + 2ºdia <= 40 questões
                    pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/rascunho_40_mais.pdf"));
                    pdfs.add(new FileInputStream(System.getProperty("user.dir")+"/src/resources/cartao_maior_40.pdf"));
                }
            }
            
                
            OutputStream output = new FileOutputStream(saida+"\\"+grupo+"\\"+aluno+".pdf");
            MergePDF.concatPDFs(pdfs, output, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void saida(ArrayList<String> arquivos,String saida,String nome) {
        try {
            List<InputStream> pdfs = new ArrayList<InputStream>();

            for(int i=0;i<arquivos.size();i++){
                pdfs.add(new FileInputStream(arquivos.get(i)));
            }
            
            OutputStream output = new FileOutputStream(saida+"\\"+nome+".pdf");
            MergePDF.concatPDFs(pdfs, output, true);
            
            for(int i=0;i<arquivos.size();i++){
                File excluir = new File(arquivos.get(i));
                excluir.delete();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void concatPDFs(List<InputStream> streamOfPDFFiles,
        OutputStream outputStream, boolean paginate) {
        Document document = new Document();
        try {
            List<InputStream> pdfs = streamOfPDFFiles;
            List<PdfReader> readers = new ArrayList<PdfReader>();
            int totalPages = 0;
            Iterator<InputStream> iteratorPDFs = pdfs.iterator();
            // Create Readers for the pdfs.
            while (iteratorPDFs.hasNext()) {
                InputStream pdf = iteratorPDFs.next();
                PdfReader pdfReader = new PdfReader(pdf);
                readers.add(pdfReader);
                totalPages += pdfReader.getNumberOfPages();
            }
            // Create a writer for the outputstream
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);
            document.open();
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA,
                    BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            PdfContentByte cb = writer.getDirectContent(); // Holds the PDF
            // data
            PdfImportedPage page;
            int currentPageNumber = 0;
            int pageOfCurrentReaderPDF = 0;//Aqui e essencial
            Iterator<PdfReader> iteratorPDFReader = readers.iterator();
            // Loop through the PDF files and add to the output.
            while (iteratorPDFReader.hasNext()) {
                PdfReader pdfReader = iteratorPDFReader.next();
                // Create a new page in the target for each source page.
                while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {
                    document.newPage();
                    pageOfCurrentReaderPDF++;
                    currentPageNumber++;
                    page = writer.getImportedPage(pdfReader,
                            pageOfCurrentReaderPDF);
                    cb.addTemplate(page, 0, 0);
                    // Code for pagination.
                    if (paginate) {
                        /*
                        cb.beginText();
                        cb.setFontAndSize(bf, 9);
                        cb.showTextAligned(PdfContentByte.ALIGN_CENTER, ""
                                + currentPageNumber + " of " + totalPages, 520,
                                5, 0);
                        cb.endText();
                        */
                    }
                }
                pageOfCurrentReaderPDF = 0;
            }
            outputStream.flush();
            document.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (document.isOpen())
                document.close();
            try {
                if (outputStream != null)
                    outputStream.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }            
        }
    }
    
    public String codigoDisciplina(String s){
        //Substring só do código da disciplina
        s = s.substring(s.lastIndexOf("\\")+1, s.lastIndexOf("Numer"));
        return s;
    }
}
